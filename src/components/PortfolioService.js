import axios from 'axios'

const PORT_REST_API_URL = 'http://localhost:8080/api/stocks/update'
class PortfolioService{

    getUsers(){
        return axios.get(PORT_REST_API_URL);
    }
}
export default new PortfolioService();